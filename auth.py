import json

class DBConection:
    def get_user(self, id):
        user = {
            "id": 1,
            "username": "ab1",
            "grants": [
                "",
                "/me",
                "/ab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3"
            ],
            "is_active": True
        }
        return json.dumps(user)

class Auth:
    db = DBConection()

    def validate(self, id, username, url):
        user = json.loads(self.db.get_user(id))

        if id is None or id <= 0:
            raise Exception('Invalid \'id\' value: ' + str(id))
        elif username is None or len(username) <3 or len(username) > 15:
            raise Exception('Invalid \'username\' length: ' + str(username))
        elif url is None or len(url) > 100:
            raise Exception('Invalid \'url\' length: ' + str(url))
        
        return "id" in user and user["id"] == id and \
            "username" in user and user["username"] == username and \
            url in user["grants"] and \
            user["is_active"]
