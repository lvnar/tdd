import unittest
from auth import Auth

testArr = [
    [1, "ab1", "", True],
    [-1, "ab1", "", None],
    [None, "ab1", "", None],
    [1, "ab1cd2ef3gh4ij5", "", False],
    [1, "ab", "", None],
    [1, None, "", None],
    [1, "ab1cd2ef3gh4ij5k", "", None],
    [1, "ab1", "/ab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3", True],
    [1, "ab1", None, None],
    [1, "ab1", "/ab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3g", None],
    [1, "ab1cd2ef3gh4ij5", "/ab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3", None],
    [-1, "ab1cd2ef3gh4ij5k", "/ab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3gab1cd2ef3g", None]
]

class TestUsers(unittest.TestCase):
    authClass = Auth()
    def test_result(self):
        for elem in testArr:
            if elem[3] is not None:
                self.assertEqual(
                    self.authClass.validate(
                        elem[0],
                        elem[1],
                        elem[2]
                    ),
                    elem[3]
                )
            else:
                try:
                    self.authClass.validate(
                        elem[0],
                        elem[1],
                        elem[2]
                    )
                except Exception as e:
                    print("Data:", elem, "Exception:", e)
